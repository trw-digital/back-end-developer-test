﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using Newtonsoft.Json;
using Trw.DeveloperTest.Models.Api;

namespace Trw.DeveloperTest.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {
            return View();
        }

        public async Task<ActionResult> Search()
        {
            // Show a UI message if there is an explicit track in the album
            SearchResponse resp = new SearchResponse();

            using (var client = new HttpClient())
            {
                var MySearch = "TRW";
                client.BaseAddress = new Uri("https://api.spotify.com/");

                HttpResponseMessage response = await client.GetAsync("v1/search?q=" + MySearch + "&type=album&market=GB");
                if (response.IsSuccessStatusCode)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    resp = JsonConvert.DeserializeObject<SearchResponse>(json);
                }
            }
            return View(resp.albums);  
        }

        public async Task<ActionResult> Album()
        {
            //GET https://api.spotify.com/v1/albums/{id}
            // List only the explicit tracks.
            return View();
        }

        public async Task<ActionResult> Listen()
        {
            //Listen to a preview of the Track.
            return View();
        }
    }
}
