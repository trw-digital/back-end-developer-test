# TRW Back End Developer Test
We are building a TRW branded Spotify search engine. But we need your help. We've built a spike, the lovely front-end team has hand crafted some templates, but we're having trouble wiring it all up to the Spotify API.

###What you need to do...

  - Clone this repo containing our spike.
  - Make it work inline with the specification below.
  - Go for any optional Bonus Points if you have time.
  - Once you are happy with your code, zip it up and send it to karl.loudon@t-rw.com
  - Bingo!
  
###Bonus Points
 1. Refactor the code so that it is more modular and reusable.
 2. Make the search AJAX rather than a full page refresh.
 3. Create a drop down from the search box which previews results as the user types.


###How long will all this take?
 Beautiful things take time, we know this, so take as much or as little time as you want. All we ask is that you let us know how long you spent. 

###What we are looking for
Hopefully this test allow you to show us the following;

1. You can read documentation (this included!) and follow it.
2. Your command of the MVC pattern.
3. Your ability to improve spike code.
4. Your ability to format code and name variables.
5. How useful your Git commit messages are

###Specification for TRW/Spotify search

#####Home Page
Currently, the home page search only returns search results for albums with the search "TRW". The home page should:

1. Allow users to search for anything, passing the users query to the controller and returning the appropriate albums to the view.


#####Search 
The Search results page should

  1. Highlight albums where any track has explicit content.
  2. Provide a link to an album page.
  3. Display something to the user stating what they searched for.
 
#####Album
The album page should

 1. List all tracks on the album.
 2. Provide a "Preview" link which allows users to listen to a preview of each track.
 
###Spotify documentation
All the documentatiaon for the Spotify API can be found over at
- https://developer.spotify.com/web-api/endpoint-reference/